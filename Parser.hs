{-# language OverloadedStrings #-}

module Parser (Module(..), TopLevel(..), Expr(..), Lit(..), Operator(..), module') where

import Control.Applicative
import Data.Foldable(asum)
import Data.Foldable(toList)
import Data.Functor.Identity
import Data.List.NonEmpty(NonEmpty(..))
import Text.Parsec hiding ((<|>), some, many)
import qualified Text.Parsec.Token as T
import qualified Text.PrettyPrint.ANSI.Leijen as P

import Operator

data Module
  = Module String [TopLevel]
  deriving (Eq, Show)
data TopLevel =
  TopLevel String Expr
  deriving (Eq, Show)
data Expr
  = Let String Expr Expr -- `let <identifier> = <expr> in <expr>
  | Lam (NonEmpty String) Expr -- `\<names> -> <expr>
  | App Expr (NonEmpty Expr) -- `([<expr>]+)`
  | BinOp Operator Expr Expr -- `(<expr> <binop> <expr>)`
  | If Expr Expr Expr -- `if <expr> then <expr> else <expr>`
  | Var String -- `<var>`
  | Lit Lit -- `[0-9]+`
  deriving (Eq, Show)
data Lit = IntLit Integer
  deriving (Eq, Show)

instance P.Pretty Module where
  pretty (Module n ts) = P.vsep $
    [P.hsep ["module", P.pretty n, "where"]] ++ (P.pretty <$> ts)
instance P.Pretty TopLevel where
  pretty (TopLevel n rhs) =
    P.hsep [P.pretty n, "=", P.pretty rhs]
instance P.Pretty Lit where
  pretty (IntLit i)
    | i < 0 = P.parens (P.pretty i)
    | otherwise = P.pretty i
instance P.Pretty Operator where
  pretty Assign = ":="
  pretty Plus = "+"
  pretty Minus = "-"
instance P.Pretty Expr where
  pretty (Let name rhs body) = P.parens $ P.hsep
      [ "let", P.pretty name, "=", P.pretty rhs, "in", P.pretty body]
  pretty (Lam args body) = P.parens $ P.hsep
      ["\\", P.hsep (toList $ P.pretty <$> args), "->", P.pretty body]
  pretty (App f es) = P.parens $ P.hsep $
      toList $ P.pretty <$> (f:|toList es)
  pretty (BinOp op l r) = P.parens $ P.hsep
      [P.pretty l, P.pretty op, P.pretty r]
  pretty (If cond then' else') = P.parens $ P.vsep
      ["if", P.pretty cond, "then", P.pretty then', "else", P.pretty else']
  pretty (Lit l) = P.pretty l
  pretty (Var n) = P.pretty n

ld :: Stream s m Char => T.GenTokenParser s u m
ld = T.makeTokenParser $ T.LanguageDef
  { T.commentStart = ""
  , T.commentEnd = ""
  , T.commentLine = "--"
  , T.nestedComments = False
  , T.identStart = letter <|> char '_'
  , T.identLetter = alphaNum <|> oneOf "\'_"
  , T.opStart = oneOf ":!#$%&*+./<=>?@\\^|-~"
  , T.opLetter = oneOf ":!#$%&*+./<=>?@\\^|-~"
  , T.reservedNames = ["module", "where", "let", "in", "if", "else"]
  , T.reservedOpNames = ["+", "-", "=", ":=", "->"]
  , T.caseSensitive = True
  }

type Parser a = ParsecT String () Identity a

parens :: Parser a -> Parser a
parens = T.parens ld
identifier :: Parser String
identifier = T.identifier ld
reserved :: String -> Parser ()
reserved = T.reserved ld
reservedOp :: String -> Parser ()
reservedOp = T.reservedOp ld
operator :: Parser String
operator = T.operator ld

module' = do
  reserved "module"
  Module <$> (identifier <* reserved "where") <*> many toplevel

toplevel = do
  name <- (identifier <?> "top level name")
  reservedOp "="
  rhs <- expr
  return $ TopLevel name rhs

expr = atomicExpr <|> parens (asum
  [ (letExpr <?> "let binding")
  , appExpr
  , (ifExpr <?> "if expression")
  , (lamExpr <?> "lambda")
  , (negativeInteger <?> "negative integer literal")
  ])

letExpr = do
  reserved "let"
  name <- identifier
  reservedOp "="
  rhs <- expr
  reserved "in"
  body <- expr
  return $ Let name rhs body

ifExpr = do
  reserved "if"
  cond <- expr
  reserved "then"
  then' <- expr
  reserved "else"
  else' <- expr
  return $ If cond then' else'

appExpr = do
  lhs <- expr
  op lhs <|> (app lhs <?> "function application")
  where
  op lhs = do
    op <- binOp
    rhs <- expr
    return $ BinOp op lhs rhs
  app lhs = do
    args <- some1 expr
    return $ App lhs args

atomicExpr =
  Var <$> identifier <|> Lit <$> lit

negativeInteger = Lit <$> IntLit <$> (reservedOp "-" *> (negate <$> T.natural ld))

lit =
  (IntLit <$> T.natural ld) <?> "positive integer literal"

binOp = Minus <$ reservedOp "-" <|> Plus <$ reservedOp "+" <|> Assign <$ reservedOp ":="
lamExpr = do
  reservedOp "\\"
  names <- some1 identifier
  reservedOp "->"
  body <- expr
  return $ Lam names body
some1 p = (:|) <$> p <*> many p

