module Name(Name(..)) where

import Data.Function(on)
import Data.Text(Text)

data Name = Name Text Int

instance Eq Name where
  Name _ i == Name _ i' = i == i'

instance Ord Name where
  compare = compare `on` (\(Name _ i) -> i)