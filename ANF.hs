module ANF(Lit(..), LetBind(..), Exp(..), CExp(..), AExp(..)) where

import Name

data Lit = IntLit Int

data LetBind = LetBind Name CExp
data Exp = Let [LetBind] CExp
data CExp = AExp AExp | App AExp AExp | If AExp Exp Exp | Lam [Name] Exp
data AExp = Var Name | Lit Lit

------------------
