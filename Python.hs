module Python ( exp', aexp, cexp ) where

import qualified Data.Text as Text

import qualified Language.Python.Common as P
import qualified ANF as ANF
import Name

exp' :: ANF.Exp -> P.Suite ()
exp' (ANF.Let bs ce) = undefined

cexp :: ANF.CExp -> P.Statement ()
cexp = undefined

aexp :: ANF.AExp -> P.Expr ()
aexp (ANF.Var n) = P.Var (name n) ()
  where
  name (Name t _) = P.Ident (Text.unpack t) ()
aexp (ANF.Lit l) = lit l
  where
  lit (ANF.IntLit i) = P.Int (fromIntegral i) (show i) ()