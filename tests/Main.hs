module Main(main) where

import Control.Applicative((<|>))
import Data.Char(toLower)
import Data.List.NonEmpty(NonEmpty(..))
import Text.Parsec(eof, runParser)
import qualified Text.PrettyPrint.ANSI.Leijen as Print
import qualified Test.QuickCheck as Q

import PredicateTransformers
import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck

import Operator
import qualified Parser as Parser

import Debug.Trace

parsePrint :: Parser.Module -> Q.Property
parsePrint expected =
    Q.property (actual == Right expected)
    where
    actual = runParser (Parser.module' <* eof) () "<test>" (printString expected)
    printString a = show $ Print.pretty a

main :: IO ()
main = defaultMain tests

identifier :: Q.Gen String
identifier =
  Q.resize 10 rawIdent `Q.suchThat` notKeyword
  where
  notKeyword = not . flip elem ["module", "where", "let", "in", "if", "else"] . fmap toLower
  rawIdent = (:) <$> identStart <*> Q.listOf identLetter
  identStart = Q.elements $ ['a'..'z'] ++ ['A'..'Z'] ++ "_"
  identLetter = Q.elements $ ['a'..'z'] ++ ['A'..'Z'] ++ "\'_" ++ ['0'..'9']

instance Q.Arbitrary Operator where
    arbitrary = Q.elements [Assign, Plus, Minus]

nel :: Q.Gen a -> Q.Gen (NonEmpty a)
nel g = (:|) <$> g <*> Q.listOf g

expr :: Q.Gen Parser.Expr
expr = expr' 5
    where
    leaves = [Parser.Var <$> identifier, Parser.Lit <$> (Parser.IntLit <$> Q.arbitrary)]
    expr' 0 = Q.oneof leaves
    expr' n = Q.oneof $
        [ Parser.Let <$> identifier <*> rec <*> rec
        , Parser.Lam <$> nel identifier <*> rec
        , Parser.App <$> rec <*> nel rec
        , Parser.BinOp <$> Q.arbitrary <*> rec <*> rec
        , Parser.If <$> rec <*> rec <*> rec
        ] ++ leaves
        where
        rec = expr' (n-1)

toplevel :: Q.Gen Parser.TopLevel
toplevel = do
    bindingName <- identifier
    rhs <- expr
    return $ Parser.TopLevel bindingName rhs

module' :: Q.Gen Parser.Module
module' = do
    moduleName <- identifier
    toplevels <- Q.resize 3 $ Q.listOf toplevel
    return $ Parser.Module moduleName toplevels

tests = testGroup "scratch ML tests" [
    testGroup "parser tests"
        [   testProperty "(parse . print) == Right" (forAll module' parsePrint)
        ]
    ]
