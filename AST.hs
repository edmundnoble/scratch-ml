module AST(Module(..), TopLevel(..), Expr(..), Lit(..), Operator(..)) where

import Operator

data Module
  = Module String [TopLevel]
  deriving Show
data TopLevel
  = TopLevel String Expr
  deriving Show
data Expr
  = Let String Expr Expr -- `let <identifier> = <expr> in <expr>
  | Lam String Expr -- `\<names> -> <expr>
  | App Expr Expr -- `([<expr>]+)`
  | BinOp Operator Expr Expr -- `<expr> <binop> <expr>`
  | If Expr Expr Expr -- `if <expr> then <expr> else <expr>`
  | Var String -- `<var>`
  | Lit Lit -- `[0-9]+`
  deriving Show
data Lit = IntLit Integer
  deriving Show
