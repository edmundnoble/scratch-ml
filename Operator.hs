module Operator(Operator(..)) where

data Operator = Plus | Minus | Assign
  deriving (Eq, Show)
