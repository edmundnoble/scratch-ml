module Type
  ( Type(..)
  , Module(..)
  , TopLevel(..)
  , Expr(..)
  ) where

import Control.Monad.IO.Class
import Control.Monad.Except
import Control.Monad.State
import Data.Map(Map)
import Data.Set(Set)
import Data.Unique
import qualified Data.Map as Map
import qualified Data.Set as Set
import qualified AST as AST
import Operator

data Module
  = Module String [TopLevel]
  deriving Show
data TopLevel
  = TopLevel String Expr
  deriving Show
data Expr
  = Let String TypeScheme Expr Expr -- `let <identifier> = <expr> in <expr>
  | Lam String Type Expr -- `\<names> -> <expr>
  | App Expr Expr -- `([<expr>]+)`
  | BinOp Operator Expr Expr -- `<expr> <binop> <expr>`
  | If Expr Expr Expr -- `if <expr> then <expr> else <expr>`
  | Var String TypeScheme -- `<var>`
  | Lit AST.Lit -- `[0-9]+`
  | TypeAbs String Expr
  | TypeInst Expr Type
  deriving Show

data Type
  = IntType -- `Int`
  | BoolType -- `Bool`
  | UnitType -- `Unit`
  | (:~>) Type Type -- `a -> b`
  | TyVar String -- `a`
  | RefType Type -- `Ref a`
  deriving (Eq, Show)

data TypeScheme
  = TyForall [String] Type
  deriving (Eq, Show)

infixr :~>

class Ops a where
  ftv :: a -> Set String
  applySubst :: Subst -> a -> a

instance Ops Type where
  ftv IntType = Set.empty
  ftv BoolType = Set.empty
  ftv ((ftv -> a) :~> (ftv -> b)) = Set.union a b
  ftv (TyVar v) = Set.singleton v
  ftv (RefType (ftv -> t)) = t
  applySubst _ IntType = IntType
  applySubst _ BoolType = BoolType
  applySubst s ((applySubst s -> a) :~> (applySubst s -> b)) = a :~> b
  applySubst s (TyVar v) = case Map.lookup v s of
      Nothing -> TyVar v
      Just ty -> ty
  applySubst s (RefType (applySubst s -> t)) = RefType t

instance Ops TypeScheme where
  ftv (TyForall vs (ftv -> t)) = foldr Set.delete t vs
  applySubst s (TyForall ns (applySubst s -> ty)) = TyForall ns ty

data TypeError
  = UnificationError Type Type
  | OccursCheck String Type
  | MissingVariable String
  deriving Show

-- lookup :: Env -> String -> Maybe Type

-- `[a] = [Int]`
-- -> `a = Int`

type Subst = Map String Type

freshTyVar :: IO String
freshTyVar = liftIO $ do
    u <- newUnique
    pure $ "__fresh_" <> show (hashUnique u)

composeSubsts :: Subst -> Subst -> Subst
composeSubsts s f = f <> (applySubst f <$> s)

addSubst :: MonadState Subst m => Subst -> m ()
addSubst s = modify (composeSubsts s)

occurs :: String -> Type -> Bool
occurs v (TyVar v') = v == v'
occurs _ IntType = False
occurs _ BoolType = False
occurs v (RefType (occurs v -> t)) = t
occurs v ((occurs v -> l) :~> (occurs v -> r)) = l || r

unify :: (MonadState Subst m, MonadError TypeError m) => Type -> Type -> m ()
unify t1 t2 | t1 == t2 = pure ()
unify (TyVar v) tv@(TyVar v') = addSubst (Map.singleton v tv)
unify (TyVar v) t2 | occurs v t2 = throwError $ OccursCheck v t2
-- `a -> b` ~ `c -> d`
unify (a :~> b) (c :~> d) = do
    unify a c
    s1 <- get
    unify (applySubst s1 b) (applySubst s1 d)
unify (RefType t) (RefType t') =
    unify t t'
unify (TyVar v) BoolType = addSubst (Map.singleton v BoolType)
unify (TyVar v) IntType = addSubst (Map.singleton v IntType)
unify (TyVar v) (RefType t) = addSubst (Map.singleton v (RefType t))
unify t1 t2 = throwError $ UnificationError t1 t2

instantiate :: TypeScheme -> IO Type
instantiate (TyForall ns ty) = do
    subst <- Map.fromList <$> traverse (\n -> (n,) <$> TyVar <$> freshTyVar) ns
    return $ applySubst subst ty

-- `a -> b` <- `forall a b. a -> b`
generalize ty = TyForall (Set.toList (ftv ty)) ty

inferLit (AST.IntLit _) = IntType

inferOp Plus = TyForall [] (IntType :~> IntType :~> IntType)
inferOp Minus = TyForall [] (IntType :~> IntType :~> IntType)
inferOp Assign = TyForall ["a"] $ (RefType (TyVar "a") :~> TyVar "a" :~> UnitType)

infer
  :: (MonadState Subst m, MonadIO m, MonadError TypeError m)
  => Map String TypeScheme -> AST.Expr -> m TypeScheme
infer env e = do
  ty <- go env e
  subst <- get
  return $ applySubst subst ty
  where
  go env = \case
    AST.Var (flip Map.lookup env -> Just t) -> pure t
    AST.Lit lit -> pure $ TyForall [] (inferLit lit)
    AST.If c t e -> do
        ct <- infer env c
        tvar <- liftIO freshTyVar
        tt <- infer env t
        et <- infer env e
        ct' <- liftIO $ instantiate ct
        tt' <- liftIO $ instantiate tt
        et' <- liftIO $ instantiate et
        unify (BoolType :~> TyVar tvar :~> TyVar tvar) (ct' :~> tt' :~> et')
        pure $ TyForall [] (TyVar tvar)
    AST.BinOp op l r -> do
        opTy <- liftIO $ instantiate $ inferOp op
        lt <- infer env l
        rt <- infer env r
        tvar <- liftIO freshTyVar
        lt' <- liftIO $ instantiate lt
        rt' <- liftIO $ instantiate rt
        unify (lt' :~> rt' :~> TyVar tvar) opTy
        pure $ TyForall [] (TyVar tvar)
    AST.App fun arg -> do
        tvar <- liftIO freshTyVar
        ft <- infer env fun
        at <- infer env arg
        ft' <- liftIO (instantiate ft)
        at' <- liftIO (instantiate at)
        unify (at' :~> TyVar tvar) ft'
        pure (TyForall [] (TyVar tvar))
    AST.Lam param body -> do
        -- param type :~> body type
        tvar <- liftIO freshTyVar
        bt <- infer (Map.insert param (TyForall [] $ TyVar tvar) env) body
        bt' <- liftIO $ instantiate bt
        pure (TyForall [] (TyVar tvar :~> bt'))
    -- `let x = y + 1 in f y x`
    AST.Let name rhs body -> do
        tvar <- liftIO freshTyVar
        rt <- infer (Map.insert name (TyForall [] $ TyVar tvar) env) rhs
        rt' <- liftIO $ instantiate rt
        unify (TyVar tvar) rt'
        let rt'' = generalizeIfValue rhs (TyVar tvar)
        bt <- infer (Map.insert name rt'' env) body
        pure bt
    where
    generalizeIfValue e ty | isValue e = generalize ty
    generalizeIfValue _ ty = TyForall [] ty
    isValue (AST.Let _ r b) = isValue r && isValue b
    isValue (AST.Lam _ _) = True
    isValue (AST.App _ _) = False
    isValue (AST.BinOp _ _ _) = False
    isValue (AST.If _ _ _) = False
    isValue (AST.Lit _) = True
