module Desugar(module', toplevel, expr) where

import Data.Unique

import AST
import qualified Parser as P
import Operator

module' :: P.Module -> Module
module' (P.Module n (fmap toplevel -> tls)) =
  Module n tls

toplevel :: P.TopLevel -> TopLevel
toplevel (P.TopLevel n (expr -> e)) =
  TopLevel n e

expr :: P.Expr -> Expr
expr (P.Let n (expr -> r) (expr -> b)) =
  Let n r b
-- Lam [a, b, c] body
-- Lam a (Lam b (Lam c body)))
expr (P.Lam ns (expr -> b)) =
  foldr Lam b ns
-- `(f a b c)`
-- `(((f a) b) c)`
expr (P.App (expr -> f) (fmap expr -> es)) =
  foldl App f es
expr (P.BinOp op (expr -> lhs) (expr -> rhs)) =
  BinOp op lhs rhs
expr (P.If (expr -> c) (expr -> t) (expr -> e)) =
  If c t e
expr (P.Var n) =
  Var n
expr (P.Lit l) =
  Lit (lit l)
  where
  lit (P.IntLit i) = IntLit i

-- parse . print = Right
